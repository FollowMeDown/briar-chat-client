import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import messagesReducer from "../reducers/messagesReducer";

export default createStore(messagesReducer, applyMiddleware(thunk));
