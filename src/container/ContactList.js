import React, { Component } from 'react';
import axios from 'axios';

export default class ContactList extends Component {
  state  = {contacts: []}
  componentDidMount() {
    axios({
      method: "GET",
      url: "http://127.0.0.1:7000/v1/contacts",
      headers: {
        "Authorization": `Bearer ${process.env.BRIAR_AUTH_TOKEN}`,
      }
    }).then(res => this.setState(
      {contacts: res.data}
    ));
  }
  render() {
    const { contacts } = this.state;
    let elements;
    if (contacts.length > 0) {
      elements = contacts.map(contact => {
        return <li key={contact.author.id}>{contact.author.name}</li>
      });
    }
   return <div className="contactlist">
   <ul className="contactlist__item">{elements}</ul>
   </div>
  }
}
