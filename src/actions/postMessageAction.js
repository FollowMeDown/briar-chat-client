import { ADD_MESSAGE } from "../types/index";
import axios from "axios";

export let addMessage = message => ({
  type: ADD_MESSAGE,
  payload: message
});

export let postMessage = message => dispatch => {
  axios({
    method: "POST",
    url: "http://127.0.0.1:7000/v1/messages/2",
    headers: {
      Authorization: `Bearer ${process.env.BRIAR_AUTH_TOKEN}`
    },
    data: {
      text: message
    }
  }).then(res => dispatch(addMessage(res.data)));
};
